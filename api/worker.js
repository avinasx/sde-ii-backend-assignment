const {parentPort} = require('worker_threads');
let Sails = require('sails');
Sails.load();
parentPort.on('message', async (rows) => {
    const processedRows = [];
    for (const row of rows) {
        console.log('row', row);
        const processedRow = await processRow(row);
        processedRows.push(processedRow);
    }
    console.log('processedRows',processedRows)
    parentPort.postMessage(processedRows);
})

async function processRow(row) {

    // let processedData = row;

    // await new Promise(resolve => setTimeout(resolve, 500));


    // Calling helpers [skipping, as it is difficult to load helpers here]
    let processingComplete = false;
    await Sails.helpers.sendForProcessing(row)
    // Wait for processing to complete
    while (!processingComplete) {

        const response = await Sails.helpers.getData.with({id: JSON.stringify(row)});
        console.log('response.status', response.status);
        if (response.status === 'complete') {
            processingComplete = true;
            processedData = response.data ?? row;
        } else {
            // Wait for some time before polling again
            await new Promise(resolve => setTimeout(resolve, 5000));
            row['Image Urls'] = 'incomplete';
            processedData = response.data ?? row;
        }
    }
    return processedData;
}