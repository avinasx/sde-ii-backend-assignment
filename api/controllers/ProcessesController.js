/**
 * ProcessesController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

const path = require("path");
const fs = require("fs");
const csv = require("csv-parser");
module.exports = {
    getRequestDetail: async function (req, res) {
        // Handle file upload

        try {
            const id = req.param('id');
            let result = await Processes.findOne({id: id});
            if(result){
                return res.ok({
                    id: result['id'],
                    status: result['status']+' %',
                    timeTaken : result['timeTaken']+' seconds',
                    completedAt: new Date(result['updatedAt']),
                    processedFilePath: req.baseUrl+result['filePath'],
                });
            }else{
                return res.notFound();
            }
        }catch (e) {
            return res.serverError(e);
        }
    }

};

