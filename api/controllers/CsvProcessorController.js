// api/controllers/CsvProcessorController.js
const csv = require('csv-parser');
const fs = require('fs');
const path = require('path');
const { createObjectCsvWriter } = require('csv-writer');
const os = require('os');
const { Worker } = require('worker_threads');

module.exports = {
  uploadAndProcessCsv: async function (req, res) {
    // Handle file upload
    req.file('csvFile').upload({
      // Set a temporary upload directory
      dirname: path.resolve(sails.config.appPath, 'assets/uploads')
    }, async function (err, uploadedFiles) {
      if (err) {
        return res.serverError(err);
      }
      
      if (uploadedFiles.length === 0) {
        return res.badRequest('No file was uploaded');
      }

      const csvFilePath = uploadedFiles[0].fd;
      
      // Read and process CSV file
      const rows = [];

      fs.createReadStream(csvFilePath)
        .pipe(csv())
        .on('data', (row) => {
          rows.push(row);
        })
        .on('end', async () => {
          try {

            // Old  Method
            // for (const row of rows) {
            //   const processedRow = await processRow(row);
            //   processedRows.push(processedRow);
            // }


            const processCreated = await Processes.create().fetch();
            const pid = processCreated.id;
            processCSV(rows, pid).then(r =>{console.log('sent')} )
            return res.ok({
              message: 'CSV is getting processed',
              request_id: pid,
            });
          } catch (error) {
            return res.serverError(error);
          }
        });
    });
  }
};

function chunked(array){
  const cpuCount = os.cpus().length;
  console.log(`cpu count: ${cpuCount}`);
  let chunks = [];
  for(let i = cpuCount; i>0 ; i--){
    chunks.push(array.splice(0,Math.ceil(array.length/i)));
  }
  return chunks;
}

async function processCSV(rows, pid){
  let originalRowLength = rows.length
  const chunks = chunked(rows)
  const processedRowsAll = [];
  let status = 0;
  const start = performance.now();
  for (const data of chunks) {
    if(!data.length){
      continue;
    }
    const worker = new Worker("./api/worker.js");
    await worker.postMessage(data);
    await worker.on('message', async (processedRow) => {
      console.log('worker finished', processedRow);
      processedRowsAll.push(...processedRow);
      status =(processedRowsAll.length/originalRowLength)*100;
      console.log('status', status);
      await Processes.updateOne({id:pid}).set({status: status});
      if (processedRowsAll.length === originalRowLength) {
        const csvPathRaw = '/uploads/processed_data_'+Math.floor(Math.random() * 1000) + '_' + Date.now() + '.csv';
        const processedCsvPath = path.resolve(sails.config.appPath, 'assets' + csvPathRaw);
        await writeProcessedDataToCsv(processedRowsAll, processedCsvPath);
        const timeTaken =  performance.now() - start
        await Processes.updateOne({id:pid}).set({filePath: csvPathRaw, timeTaken: timeTaken});
        // process.exit();
        worker.terminate()
      }
    })
  }
}

async function writeProcessedDataToCsv(processedRowsAll, filePath) {
  console.log('filewriting',processedRowsAll)
  const csvWriter = createObjectCsvWriter({
    path: filePath,
    header: Object.keys(processedRowsAll[0]).map(key => ({ id: key, title: key }))
  });
  await csvWriter.writeRecords(processedRowsAll);
}
