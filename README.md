# my-api

a [Sails v1](https://sailsjs.com) application


### Links

+ [Sails framework documentation](https://sailsjs.com/get-started)
+ [Version notes / upgrading](https://sailsjs.com/documentation/upgrading)
+ [Deployment tips](https://sailsjs.com/documentation/concepts/deployment)
+ [Community support options](https://sailsjs.com/support)
+ [Professional / enterprise options](https://sailsjs.com/enterprise)


### Version info

This app was originally generated on Tue May 28 2024 10:04:16 GMT+0530 (India Standard Time) using Sails v1.5.8.

<!-- Interlly, Sails used [`sails-generate@2.0.9`](https://github.com/balderdashy/sails-generate/tree/v2.0.9/lib/core-generators/new). --

<!--
Note:  Generators are usually run using the globally-installed `sails` CLI (command-line interface).  This CLI version is _environment-specific_ rather than app-specific, thus over time, as a project's dependencies are upgraded or the project is worked on by different developers on different computers using different versions of Node.js, the Sails dependency in its package.json file may differ from the globally-installed Sails CLI release it was originally generated with.  (Be sure to always check out the relevant [upgrading guides](https://sailsjs.com/upgrading) before upgrading the version of Sails used by your app.  If you're stuck, [get help here](https://sailsjs.com/support).)
-->


Step to solution:
1. change the database url in config (mysql/mariadb)
2. run migration
3. run the dev server ``sails lift``
4. API Documentation:
   https://documenter.getpostman.com/view/11280721/2sA3XJn5yR
5. HLD:
   https://docs.google.com/document/d/1OIZUYoH57RFQu2wxlObzYvzzZcjnCStBNTYGEUA1C70/edit?usp=sharing
 

