// config/routes.js
module.exports.routes = {
  'POST /upload-csv': 'CsvProcessorController.uploadAndProcessCsv',
  'GET /request-status': 'ProcessesController.getRequestDetail'
};
